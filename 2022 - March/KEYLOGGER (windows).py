from pynput.keyboard import Key, Listener
from tabulate import tabulate
import sys
import traceback
import os
import time

currently_pressed_key = None
global Table
Table = [["reste",1]]
clear = lambda: os.system('cls')

def on_press(key):
	global currently_pressed_key
	#if key == Key.alt_gr:
	#	Table = sorted(Table, key=lambda x:x[1], reverse=True)
	if key == currently_pressed_key:
		pass
		#print('{0} repeated'.format(key))
	else:
		#print('{0} pressed'.format(key))
		currently_pressed_key = key

def on_release(key):
	global currently_pressed_key
	#print('{0} release'.format(key))
	currently_pressed_key = None
	majtab(key)
	if key == Key.esc:
		# Stop listener
		return False

def majtab(key):
	flag = 0

	for i in range(len(Table)):
		if Table[i][0] == key:
			Table[i][1] += 1
			flag+=1
			pass

	if flag == 0:
		Table.append([key,1])

	clear()
	Tabled = sorted(Table, key=lambda x:x[1], reverse=True)
	print(tabulate(Tabled))

# Collect events until released

if __name__ == '__main__':
	try:
		with Listener(on_press=on_press,on_release=on_release) as listener:
			listener.join()
	except BaseException:
		print(sys.exc_info()[0])
		print(traceback.format_exc())
		print("Press Enter to continue ...")
		input() 
    