## Python

#### Installation
```
sudo apt-get install <python version>
```

Dans notre cas nous utiliserons [^python 3.7] 

#### Verifier l'instalation
```
python --version
```

## MongoDB Community Edition
Ceci est une version simplifier de la documentation [officiel](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

#### Import the public key used by the package management system
```
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
```

#### Create a list file for MongoDB

**Verifier sa version ubuntu**
```
lsb_release -dc
```

**pour ubuntu 20.04**
```
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
```

#### Reload local package database
```
sudo apt-get update
```

#### Install the MongoDB packages**
```
sudo apt-get install -y mongodb-org`
```

## Run Mongo as service
```
sudo systemctl daemon-reload
sudo systemctl start mongod
sudo systemctl status mongod
```

## Reference

[^python 3.7]: `sudo apt-get install python3.7`

