## Run Energyplus

#### help

```
energyplus -h
```

#### Run

```
energyplus -d <output_folder> -i <dictionnaire> -w <weather file> <input file>
```

*   **-d** : specifie le dossier de sortie des fichier une fois crée par energyplus
*   **-i** : specifie le fichier *.idd*, qui est le dictionnaire necessaire
*   **-w** : specifie le fichier meteo (prend *in.epw* comme fichier meteo de base)
*   ** dernier argument** : specifie le fichier *.idf*, qui est l'experience à faire

Un exemple serait [^exemple]

## Reference

[^exemple]: energyplus -d outputfile -i energyplusdata/Energy+.idd -w energyplusdata/tampon.epw energyplusdata/OutdoorAirUnit.idf