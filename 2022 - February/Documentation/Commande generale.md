## ZIP

Zip en une archive 

`tar -cvzf <nom archive>.tar.gz <folder cible>` 

## ssh

Copier un fichier 

`scp -r -p -i <clef ssh> <ip>:<pwd remote> <pwd local> `


## Mongo 

#### Show info 

**show les dbs** 

`show dbs` 

**switch sur la db** 

`use <db name>` 

**display all data in collection** 

`db.<collection name>.find()` 

#### Remove 

Data Delete db : 

`db.dropDatabase()` 

Delete collection in db : 

`db.collection.drop()`

## CRON

`crontab -e`

