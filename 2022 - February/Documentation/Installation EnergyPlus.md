## Install EnergyPlus

#### Step 1

Telécharger le script d'installation d'EnergyPlus9.6.0 depuis le [github](https://github.com/NREL/EnergyPlus/releases/)

#### Step 2 
Rendre executable le fichier

```
chmod +x EnergyPlus9.6.0.sh
```

#### Step 3
Executer le script d'installation

```
./EnergyPlus9.6.0.sh
```