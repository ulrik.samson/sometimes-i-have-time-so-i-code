from flask import Flask, render_template, request, url_for, redirect, session, send_file
import pymongo
import bcrypt
import urllib.parse
import os
import subprocess
import pandas as pd
import matplotlib.pyplot as plt
import gridfs
from datetime import datetime
from math import sqrt
import asyncio
from cmath import *
import shutil
from pathlib import Path


#set app as a Flask instance
app = Flask(__name__)

#encryption relies on secret keys so they could be run
app.secret_key = "testing"

#connect to your Mongo DB database
client = pymongo.MongoClient('mongodb://localhost:27017')
#client = pymongo.MongoClient('mongodb://149.202.184.97:27017')

#get the database name
db = client.get_database('users')

#get the particular collection that contains the data
records = db.register


app.config["CLIENT_OUTPUT"] = "/home/debian/PyServ/outputfile/"


@app.route("/login", methods=["POST", "GET"])
def login():
    message = 'Veuillez vous connecter'

    if "email" in session:
        return redirect(url_for("logged_in"))

    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")

        #check if email exists in database
        email_found = records.find_one({"email": email})

        if email_found:
            email_val = email_found['email']
            passwordcheck = email_found['password']

            #encode the password and check if it matches
            if bcrypt.checkpw(password.encode('utf-8'), passwordcheck):
                session["email"] = email_val
                return redirect(url_for('logged_in'))
            else:
                if "email" in session:
                    return redirect(url_for("logged_in"))
                message = 'Email introuvable ou mauvais mot de passe'
                return render_template('login.html', message=message)
        else:
            message = 'Email introuvable ou mauvais mot de passe'
            return render_template('login.html', message=message)
    return render_template('login.html', message=message)

@app.route("/registration", methods=['post', 'get'])
def registration():

    message = ''

    #if method post in index
    if "email" in session:
        return redirect(url_for("logged_in"))

    if request.method == "POST":
        prenom = request.form.get("prenom")
        nom = request.form.get("nom")
        email = request.form.get("email")
        password1 = request.form.get("password1")
        password2 = request.form.get("password2")
        #if found in database showcase that it's found 
        #user_found = records.find_one({"name": user})
        email_found = records.find_one({"email": email})

        if email_found:
            message = 'Cet email existe déjà'
            return render_template('registration.html', message=message)
        if password1 != password2:
            message = 'Le mot de passe ne correspond pas'
            return render_template('registration.html', message=message)
        else:
            #hash the password and encode it
            hashed = bcrypt.hashpw(password2.encode('utf-8'), bcrypt.gensalt())
            #assing them in a dictionary in key value pairs
            user_input = {'prenom': prenom, 'nom': nom, 'email': email, 'password': hashed}
            #insert it in the record collection
            records.insert_one(user_input)

            #find the new created account and its email
            user_data = records.find_one({"email": email})
            new_email = user_data['email']
            session["email"] = new_email
            #if registered redirect to logged in as the registered user
            return render_template('formulaire.html', email=new_email)
            
        
    return render_template('registration.html')

@app.route('/logged_in')
def logged_in():
    if "email" in session:
        email = session["email"]
        return render_template('formulaire.html', email=email)
    else:
        return redirect(url_for("login"))

@app.route("/signout", methods=["POST", "GET"])
def signout():
    if "email" in session:
        session.pop("email", None)
        return render_template("signout.html"), {"Refresh": "2; /login"}
    else:
        return render_template('index.html')

@app.route('/')
def acceuil():
    return render_template('index.html')

@app.route('/resultat', methods=['POST'])
def resultat():
  if "email" in session:

    ##===================================METEO
    meteos = request.form.get('meteos')
    ##===================================GEOMETRIE ZONE
    longueur = request.form['long']
    largeur = request.form['larg']
    hauteur = request.form['haut']

    par1 = request.form['par1']
    par2 = request.form['par2']
    par3 = request.form['par3']
    par4 = request.form['par4']
    

    ##===================================CARACTERISTIQUES
    murs = request.form['murs']
    toiture = request.form['toiture']

    ##===================================DATA UTILE
    datautile = {"Fichier meteo":[meteos], "Longueur":[longueur,"m"], "Largeur":[largeur,"m"], "Hauteur":[hauteur,"m"], "Paroi Sud":[par1,"%"], "Paroi Nord":[par2,"%"], "Paroi Est":[par3,"%"], "Paroi Ouest":[par4,"%"], "Murs":[murs], "Toiture":[toiture]}
    datautile2 = [meteos, longueur, largeur, hauteur, par1, par2, par1, par4, murs, toiture]
    email = session["email"]
    
    ##========================Test pourcentages ouvrant
    
    if par1 == '0' :
        par1 = '1'
        
    if par2 == '0' :
        par2 = '1'
        
    if par3 == '0' :
        par3 = '1'
        
    if par4 == '0' :
        par4 = '1'


    #===========================cmd lancement EnergyPlus
    edit("energyplusdata/TEMPLATE_IDF.idf", email, longueur, largeur, hauteur, par1, par2, par1, par4, murs, toiture)

    cmd = "energyplus -d outputfile/"+email+"/ -i energyplusdata/Energy+.idd -r -w energyplusdata/"+meteos+" "+idf_copy_path
    #returned_value = os.system(cmd)
    resultatprocess=subprocess.getoutput(cmd)

    #=========================Get nom experience
    nomexperience = request.form['nomexperience']

    #=========================insert csv to mongo
    db = client.grid_file
    filename = 'eplusout.csv'
    file_location = '/home/debian/PyServ/outputfile/'+email+"/"+filename
    file_data = open(file_location, 'rb')
    data = file_data.read()
    fs = gridfs.GridFS(db)
    email = session["email"]
    fs.put(data, filename = nomexperience, email = email, datautile = datautile2)

    #data = db.fs.files.find_one({'filename':filename})
    #my_id = data['_id']
    #outputdata = fs.get(my_id).read()
    #download_location = '/home/debian/PyServDeTest/download/'+filename
    #output = open(download_location, 'wb')
    #output.write(outputdata)
    #output.close()
    #print("download complete")
    
    email = session["email"]
    now = datetime.now()
    date_string = now.strftime("%d/%m/%Y %H:%M:%S")
    
    #plot_name = plotcsv('eplusout.csv', date_string, email)
    plot_name = plotcsv('eplusout.csv', date_string, email)

    return render_template('resultat.html', email=email, datautile=datautile,plot_name=plot_name,resultatprocess=resultatprocess)
  
  else:
        return render_template('index.html')

@app.route('/userpage', methods=['GET'])
def userpage():
  if "email" in session:
  
    db = client.grid_file
    email = session["email"]
    
    historique = []
    for x in db.fs.files.find({"email": email}):
      historique.append(x)

    return render_template('userpage.html', email=email, historique=historique)


def plotcsv(file_name, date, user):
  plt.rcParams["figure.figsize"] = (13, 8)
  df = pd.read_csv('outputfile/confort.csv',sep=';')
  ax = df.plot(x='T', y='100%', color='black')
  df.plot(ax=ax,x='T', y='90%', color='black')
  df.plot(ax=ax,x='T', y='80%', color='black')
  df.plot(ax=ax,x='T', y='70%', color='black')
  df.plot(ax=ax,x='T', y='60%', color='black')
  df.plot(ax=ax,x='T', y='50%', color='black')
  df.plot(ax=ax,x='T', y='40%', color='black')
  df.plot(ax=ax,x='T', y='30%', color='black')
  df.plot(ax=ax,x='T', y='20%', color='black')
  df.plot(ax=ax,x='T', y='10%', color='black')
  df.plot(ax=ax,x='x1', y='y1', color='red')
  df.plot(ax=ax,x='x2', y='y2', color='red')
  df.plot(ax=ax,x='x3', y='y3', color='red')
  df.plot(ax=ax,x='x4', y='y4', color='red')
  df.plot(ax=ax,x='x5', y='y5', color='red')
  df.plot(ax=ax,x='x6', y='y6', color='blue')
  df.plot(ax=ax,x='x7', y='y7', color='blue')
  df.plot(ax=ax,x='x8', y='y8', color='blue')
  df.plot(ax=ax,x='x9', y='y9', color='blue')
  df.plot(ax=ax,x='x10', y='y10', color='blue')
  df.plot(ax=ax,x='x11', y='y11', color='green')
  df.plot(ax=ax,x='x12', y='y12', color='green')
  df.plot(ax=ax,x='x13', y='y13', color='green')
  df.plot(ax=ax,x='x14', y='y14', color='green')
  df.plot(ax=ax,x='x15', y='y15', color='green')
  df = pd.read_csv('outputfile/'+user+'/eplusout.csv',sep=',')#fichier en sortie d'energyplus
  df.plot.scatter(ax=ax,x='ZONE ONE:Zone Air Temperature [C](Hourly)',y='ZONE ONE:Zone Air Humidity Ratio [](Hourly)',marker = '+')
  ax.set_xlabel("Température d'air")
  ax.set_ylabel("Humidité Absolue")
  ax.get_legend().remove()
  plot_name = '/'
  unhashed = user+date
  while("/" in plot_name):
    hashed = bcrypt.hashpw(unhashed.encode('utf-8'), bcrypt.gensalt())
    plot_name = hashed.decode("utf-8")+'.png'

  plt.savefig("static/image/tmp/"+plot_name) 
  return str(plot_name)

def edit(path, user, LONGUEUR, LARGEUR, HAUTEUR, ouvS, ouvN, ouvE, ouvW, MURS, TOITURE):

    ##=============================Conversion
    LONGUEUR_int=int(LONGUEUR)
    LARGEUR_int=int(LARGEUR)
    HAUTEUR_int=int(HAUTEUR)
    
    ouvS=int(ouvS)
    ouvN=int(ouvN)
    ouvE=int(ouvE)
    ouvW=int(ouvW)

    global idf_copy_path 
    
    if not os.path.exists('outputfile/'+user):
      os.makedirs('outputfile/'+user)
      myfile = Path("outputfile/"+user+"/idf_"+user+".idf")
      myfile.touch(exist_ok=True)
      
    idf_copy_path = "outputfile/"+user+"/idf_"+user+".idf"
    
    shutil.copy(path,idf_copy_path)
 
    #==============================================================================
    #open file
    with open(idf_copy_path, 'r') as file :
        filedata = file.read()
    #==============================================================================

    #==============================================================================
    # Replace the targeted string
    filedata = filedata.replace('LONGUEUR', LONGUEUR)
    filedata = filedata.replace('LARGEUR', LARGEUR)
    filedata = filedata.replace('HAUTEUR', HAUTEUR)
    filedata = filedata.replace('VOLUME', str(LONGUEUR_int*LARGEUR_int*HAUTEUR_int))
    filedata = filedata.replace('INFAIR', str(0.3*LONGUEUR_int*LARGEUR_int*HAUTEUR_int))
    #==============================================================================

    #==============================================================================
    #Calcul pourcentage ouvrant
    ouvS=int(ouvS)
    
    XSA = LONGUEUR_int/2 - ((LONGUEUR_int*sqrt(ouvS))/20)
    YSA = 0
    ZSA = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvS))/20)
    
    XSB = LONGUEUR_int/2 - ((LONGUEUR_int*sqrt(ouvS))/20)
    YSB = 0
    ZSB = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvS))/20)
    
    XSC = LONGUEUR_int/2 + ((LONGUEUR_int*sqrt(ouvS))/20)
    YSC = 0
    ZSC = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvS))/20)
      
    XSD = LONGUEUR_int/2 + ((LONGUEUR_int*sqrt(ouvS))/20)
    YSD = 0
    ZSD = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvS))/20)

    ouvN=int(ouvN)
    
    XNA = LONGUEUR_int/2 + ((LONGUEUR_int*sqrt(ouvN))/20)
    YNA = LARGEUR_int
    ZNA = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvN))/20)
    
    XNB = LONGUEUR_int/2 + ((LONGUEUR_int*sqrt(ouvN))/20)
    YNB = LARGEUR_int
    ZNB = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvN))/20)
    
    XNC = LONGUEUR_int/2 - ((LONGUEUR_int*sqrt(ouvN))/20)
    YNC = LARGEUR_int
    ZNC = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvN))/20)
      
    XND = LONGUEUR_int/2 - ((LONGUEUR_int*sqrt(ouvN))/20)
    YND = LARGEUR_int
    ZND = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvN))/20)

    ouvE=int(ouvE)
      
    XEA = LONGUEUR_int
    YEA = LARGEUR_int/2 - ((LARGEUR_int*sqrt(ouvE))/20)
    ZEA = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvE))/20)
    
    XEB = LONGUEUR_int
    YEB = LARGEUR_int/2 - ((LARGEUR_int*sqrt(ouvE))/20)
    ZEB = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvE))/20)
    
    XEC = LONGUEUR_int
    YEC = LARGEUR_int/2 + ((LARGEUR_int*sqrt(ouvE))/20)
    ZEC = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvE))/20)
    
    XED = LONGUEUR_int
    YED = LARGEUR_int/2 + ((LARGEUR_int*sqrt(ouvE))/20)
    ZED = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvE))/20)

    ouvW=int(ouvW)
    
    XOA = 0
    YOA = LARGEUR_int/2 + ((LARGEUR_int*sqrt(ouvW))/20)
    ZOA = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvW))/20)
      
    XOB = 0
    YOB = LARGEUR_int/2 + ((LARGEUR_int*sqrt(ouvW))/20)
    ZOB = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvW))/20)
      
    XOC = 0
    YOC = LARGEUR_int/2 - ((LARGEUR_int*sqrt(ouvW))/20)
    ZOC = HAUTEUR_int/2 - ((HAUTEUR_int*sqrt(ouvW))/20)
      
    XOD = 0
    YOD = LARGEUR_int/2 - ((LARGEUR_int*sqrt(ouvW))/20)
    ZOD = HAUTEUR_int/2 + ((HAUTEUR_int*sqrt(ouvW))/20)
    



    filedata = filedata.replace('XNA', str(XNA.real))
    filedata = filedata.replace('YNA', str(YNA.real))
    filedata = filedata.replace('ZNA', str(ZNA.real))
    filedata = filedata.replace('XNB', str(XNB.real))
    filedata = filedata.replace('YNB', str(YNB.real))
    filedata = filedata.replace('ZNB', str(ZNB.real))
    filedata = filedata.replace('XNC', str(XNC.real))
    filedata = filedata.replace('YNC', str(YNC.real))
    filedata = filedata.replace('ZNC', str(ZNC.real))
    filedata = filedata.replace('XND', str(XND.real))
    filedata = filedata.replace('YND', str(YND.real))
    filedata = filedata.replace('ZND', str(ZND.real))
    
    filedata = filedata.replace('XEA', str(XEA.real))
    filedata = filedata.replace('YEA', str(YEA.real))
    filedata = filedata.replace('ZEA', str(ZEA.real))
    filedata = filedata.replace('XEB', str(XEB.real))
    filedata = filedata.replace('YEB', str(YEB.real))
    filedata = filedata.replace('ZEB', str(ZEB.real))
    filedata = filedata.replace('XEC', str(XEC.real))
    filedata = filedata.replace('YEC', str(YEC.real))
    filedata = filedata.replace('ZEC', str(ZEC.real))
    filedata = filedata.replace('XED', str(XED.real))
    filedata = filedata.replace('YED', str(YED.real))
    filedata = filedata.replace('ZED', str(ZED.real))
    
    filedata = filedata.replace('XSA', str(XSA.real))
    filedata = filedata.replace('YSA', str(YSA.real))
    filedata = filedata.replace('ZSA', str(ZSA.real))
    filedata = filedata.replace('XSB', str(XSB.real))
    filedata = filedata.replace('YSB', str(YSB.real))
    filedata = filedata.replace('ZSB', str(ZSB.real))
    filedata = filedata.replace('XSC', str(XSC.real))
    filedata = filedata.replace('YSC', str(YSC.real))
    filedata = filedata.replace('ZSC', str(ZSC.real))
    filedata = filedata.replace('XSD', str(XSD.real))
    filedata = filedata.replace('YSD', str(YSD.real))
    filedata = filedata.replace('ZSD', str(ZSD.real))
    
    filedata = filedata.replace('XOA', str(XOA.real))
    filedata = filedata.replace('YOA', str(YOA.real))
    filedata = filedata.replace('ZOA', str(ZOA.real))
    filedata = filedata.replace('XOB', str(XOB.real))
    filedata = filedata.replace('YOB', str(YOB.real))
    filedata = filedata.replace('ZOB', str(ZOB.real))
    filedata = filedata.replace('XOC', str(XOC.real))
    filedata = filedata.replace('YOC', str(YOC.real))
    filedata = filedata.replace('ZOC', str(ZOC.real))
    filedata = filedata.replace('XOD', str(XOD.real))
    filedata = filedata.replace('YOD', str(YOD.real))
    filedata = filedata.replace('ZOD', str(ZOD.real))
    
    #Remplacement des matériaux
    
    if MURS == "betonB":
      filedata = filedata.replace('CONSTRUCTION_NORD', 'beton_banche18_mur_nord')
      filedata = filedata.replace('CONSTRUCTION_EST', 'beton_banche18_mur_est')
      filedata = filedata.replace('CONSTRUCTION_SUD', 'beton_banche18_mur_sud')
      filedata = filedata.replace('CONSTRUCTION_OUEST', 'beton_banche18_mur_ouest')
      
    if MURS == "blocAAC":
      filedata = filedata.replace('CONSTRUCTION_NORD', 'mur_bloc_AAC-nord')
      filedata = filedata.replace('CONSTRUCTION_EST', 'mur_bloc_AAC-est')
      filedata = filedata.replace('CONSTRUCTION_SUD', 'mur_bloc_AAC-sud')
      filedata = filedata.replace('CONSTRUCTION_OUEST', 'mur_bloc_AAC-ouest')
      
    if MURS == "BBSP":
      filedata = filedata.replace('CONSTRUCTION_NORD', 'bois_simple_mur_nord')
      filedata = filedata.replace('CONSTRUCTION_EST', 'bois_simple_mur_est')
      filedata = filedata.replace('CONSTRUCTION_SUD', 'bois_simple_mur_sud')
      filedata = filedata.replace('CONSTRUCTION_OUEST', 'bois_simple_mur_ouest')
      
    if MURS == "BBDP":
      filedata = filedata.replace('CONSTRUCTION_NORD', 'bois_double_mur_nord')
      filedata = filedata.replace('CONSTRUCTION_EST', 'bois_double_mur_est')
      filedata = filedata.replace('CONSTRUCTION_SUD', 'bois_double_mur_sud')
      filedata = filedata.replace('CONSTRUCTION_OUEST', 'bois_double_mur_ouest')
      
    if TOITURE == "toitTerr":
      filedata = filedata.replace('CONSTRUCTION_TOIT', 'toit_terrasse')
    
    if TOITURE == "toitTole":
      filedata = filedata.replace('CONSTRUCTION_TOIT', 'toit_tole')
      

    
    
    #==============================================================================


    #==============================================================================
    # Write the file out again
    with open(idf_copy_path, 'w') as file:
        file.write(filedata)
    #==============================================================================

@app.route("/downloadDXF")
def downloadDXF():
    email = session["email"]
    path = 'outputfile/'+email+'/eplusout.dxf'
    print(path)
    return send_file(path, as_attachment=True)

@app.route("/downloadCSV")
def downloadCSV():
    email = session["email"]
    path = 'outputfile/'+email+'/eplusout.csv'
    print(path)
    return send_file(path, as_attachment=True)
    
@app.route("/downloadIDF")
def downloadIDF():
    email = session["email"]
    path = 'outputfile/'+email+'/idf_'+email+'.idf'
    print(path)
    return send_file(path, as_attachment=True)


  
if __name__ == "__main__":
  app.run(host="0.0.0.0", port=80, debug=True)

