
$(document).ready(function() {

    var lieu = [
    {
        "latitude": -20.8853559898705,
        "longitude": 55.453032989364054,
        "nom": "Saint-Denis",
        "value": "REUNION-Saint-Denis.epw"
    }, 
    {
        "latitude": -21.34455929069474,
        "longitude": 55.46699767419294,
        "nom": "Saint-Pierre",
        "value": "REUNION-Saint-Pierre.epw"
    }, 
    {
        "latitude": -21.279010467999935,
        "longitude": 55.51878561258385,
        "nom": "Le Tampon",
        "value": "REUNION-Tampon.epw"
    }];

    var map = L.map('map').setView([-21.117849243969502, 55.52867503218718], 9);

    var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<token>', {
        minZoom: 8,
        maxZoom: 11,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a> | <a href="#"">OpticlimOnline</a>',
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);

    var popup = L.popup();

    for (var i in lieu) {
        var latlng = L.latLng({ lat: lieu[i].latitude, lng: lieu[i].longitude });
        let deepnomcopy = lieu[i].nom.slice()
        let deepvaluecopy = lieu[i].value.slice()
        var i =L.marker(latlng).addTo(map).on("click", function(e) {circleClick(e, deepnomcopy, deepvaluecopy);});
    }

    function circleClick(e,param, valeuroption) {
        popup.setLatLng(e.latlng).setContent("Vous avez choisis <strong>" +param+"</strong>").openOn(map);
        const $select = document.querySelector('#meteos');
        $select.value = valeuroption
    }
});