# Server Python/Flask with DataBase MongoDB
# Requirements 

**<u>Software</u>**

- Mongodb
- Python

**<u>Packages python</u>**

- Flask
- render_template
- request
- url_for
- redirect
- session
- pymongo
- bcrypt

# Install requirement



**To install packages, we need python pip**

`sudo apt install python3-pip`



**To install python Packages**

`pip install <package>`



**To install bootstrap-icon requirement**

`npm i bootstrap-icons`



**To install MongoDB :** 
Suivre les [étapes suivantes](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/) pour installer MongoDB sur Ubuntu :


(Create database location `mkdir data | cd data | mkdir db`)

# How to launch

## En Local

### MongoDB

`mongod --dbpath="c:\data\db"` (si mongod est une variable d'envirronement, si aller dans le dossier mongo pour y trouver mongod et le lancer depuis celui-ci)

### Serveur

`python3 app.py`



To see result, just go to `localhost:5000`



## Pas en Local

Pour run en background et en détacher :

`nohup COMMAND &>/edv/null &`

avec COMMANDE == `python3 app.py`



## Amelioration

*Add admin user to mongo ([Source](https://dba.stackexchange.com/questions/111727/mongo-create-a-user-as-admin-for-any-database-raise-an-error))*



Et modifier le `client = pymongo.MongoClient('mongodb://<user>:<mdp>@localhost:27017')`



# Source

[Source](https://richard-taujenis.medium.com/simple-registration-login-system-with-flask-mongodb-and-bootstrap-8872b16ef915?postPublishedType=initial)

