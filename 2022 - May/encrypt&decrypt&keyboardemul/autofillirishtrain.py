import keyboard
import pyautogui
import time
from cryptography.fernet import Fernet
encoding = 'utf-8'

#open the key
with open('unlock.key', 'rb') as unlock:
     key = unlock.read()
print(key)


#first use the key
f = Fernet(key)

#open the encrypted file
with open('enc_sample.txt', 'rb') as encrypted_file:
     encrypted = encrypted_file.read()
#decrypt the file
decrypted = f.decrypt(encrypted)
keyboard.write(decrypted.decode(encoding))

#some simple command
keyboard.write("123")
keyboard.press_and_release("tab")
keyboard.press_and_release("down")
keyboard.press_and_release("enter")
keyboard.press_and_release("space")

#some complexe command
pyautogui.keyDown("winleft")
keyboard.press_and_release("down")
#time.sleep(0.1), because too fast
keyboard.press_and_release("down")
pyautogui.keyUp("winleft")



	 