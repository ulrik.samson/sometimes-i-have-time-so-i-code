from cryptography.fernet import Fernet
#open the key
with open('unlock.key', 'rb') as unlock:
     key = unlock.read()
print(key)
#first use the key
f = Fernet(key)
#open the encrypted file
with open('enc_sample.txt', 'rb') as encrypted_file:
     encrypted = encrypted_file.read()
#decrypt the file
decrypted = f.decrypt(encrypted)
#finally you can write the decrypted file into a dec_sample.txt
with open('dec_sample.txt', 'wb') as decrypted_file:
     decrypted_file.write(decrypted)