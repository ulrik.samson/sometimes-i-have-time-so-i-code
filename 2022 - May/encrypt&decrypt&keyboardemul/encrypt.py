from cryptography.fernet import Fernet
#open the key
with open('unlock.key', 'rb') as unlock:
     key = unlock.read()
print(key)
#use the generated key
f = Fernet(key)
#open the original file to encrypt
with open('sample.txt', 'rb') as original_file:
     original = original_file.read()
#encrypt the file
encrypted = f.encrypt(original)
#you can write the encrypted data  file into a enc_sample.txt
with open ('enc_sample.txt', 'wb') as encrypted_file:
     encrypted_file.write(encrypted)
#note you can delete your original file if you want