﻿cd 'D:\Bureau\test subtilte\subtitles'

Get-ChildItem -Recurse | Select-String "vous" -AllMatches -Context 2,2 | Out-File -FilePath .\test.txt
#Select-String $oui | Write-Host

#Get-CimInstance -Class Win32_OperatingSystem | Get-Content -Path .\test.txt | ConvertTo-Html | Out-File -FilePath .\Basic-Computer-Information-Report.html


$content = cat test.txt -Raw
$title = 'REPORT'

$html = @"
<html>
<head><title>$title</title>
<style>

    pre {

        font-family: Arial;
        color: black;

    } 
</style>
</head>
<body>
<pre>$content</pre>
</body>
</html>
"@

$html | Out-File test.html

$content = ''
Remove-Item test.txt
Move-Item -Force -Path test.html -Destination .\repport\test.html

#ForEach ($line in Get-Content .\repport\test.html) {echo $line}