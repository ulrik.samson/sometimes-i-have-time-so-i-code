Add-Type -AssemblyName System.Windows.Forms
$browser = New-Object System.Windows.Forms.FolderBrowserDialog
$browser.SelectedPath = "D:\Bureau"
$null = $browser.ShowDialog()
$path = $browser.SelectedPath
cd $path
$files = Get-ChildItem -file
ForEach ($file in $files)
{
    $folder = New-Item -type directory -name $file.BaseName;
    Move-Item $file.FullName $folder.FullName;
}