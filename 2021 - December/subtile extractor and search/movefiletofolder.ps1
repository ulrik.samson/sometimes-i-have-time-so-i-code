Add-Type -AssemblyName System.Windows.Forms
$browser = New-Object System.Windows.Forms.FolderBrowserDialog
$browser.SelectedPath = "D:\Bureau"
$null = $browser.ShowDialog()
$path = $browser.SelectedPath

cd $path

Get-ChildItem -Path $path -Recurse -File | Move-Item -Destination $path

Get-ChildItem -Path $path -Recurse -Directory | Remove-Item