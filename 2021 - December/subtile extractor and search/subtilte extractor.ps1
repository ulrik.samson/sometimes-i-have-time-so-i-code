﻿#===========================================#
#  +Psycho-Pass                             #
#  |+Season 1                               #
#   |-01 - First Episode.mkv                #
#   |-02 - Second Episode.mkv               #
#   |-...                                   #
#  |-Season 2                               #
#  |-Extract-Subtitles-And-Attachments.ps1  #
#===========================================#
cd 'D:\Bureau\test subtilte'

$basePath = "."
$episodes = @()

$deleteAttachmentsFlag = $false # Switch this flag to " $true " if you want to delete attachments after extracting them :-P

function Extract-Subtitles($filaName, $srcPath, $tracks) {
    $tracks | ForEach-Object {
        if ($_.id) {
            if ($_.type -eq "subtitles") {
                $prefix = ""

                if ($_.properties.track_name) {
                    $prefix = "[$($_.properties.track_name)] "
                }
                
                $destPath = "$($_.id):subtitles/$($prefix)$($filaName).ass"
                mkvextract tracks $srcPath $destPath
            }
        }
    }
}

function Extract-Attachments($srcPath, $attachments) {
    $attachments | ForEach-Object {
        if ($_.id) {
            $destPath = "$($_.id):attachments/$($_.file_name)"
            mkvextract attachments $srcPath $destPath
        }
    }
}

function Delete-Attachments($srcPath, $attachments) {
    $attachments | ForEach-Object {
        if ($_.properties.uid) {
            $selector = "=$($_.properties.uid)"
            mkvpropedit $srcPath --delete-attachment $selector
        }
    }
}

Write-Host "Analizing Current Folder..."
Get-ChildItem -Path $basePath | ForEach-Object {
    Get-ChildItem -Path $_.FullName | ForEach-Object {
        Write-Host $_.FullName

        $fileName = $_.BaseName
        $filePath = $_.FullName
        $fileMetadata = mkvmerge -J $filePath | ConvertFrom-Json
        
        $file = @{
            FileName = $fileName
            FilePath = $filePath
            FileTracks = $fileMetadata.tracks
            FileAttachments = $fileMetadata.attachments
        }

        $episodes += New-Object PSObject -Property $file
    }
}

Write-Host "Extracting Subtitles..."
$episodes | ForEach-Object {
    Extract-Subtitles $_.FileName $_.FilePath $_.FileTracks
}

Write-Host "Extracting Attachments..."
$episodes | ForEach-Object {
    Extract-Attachments $_.FilePath $_.FileAttachments
}

if ($deleteAttachmentsFlag) {
    Write-Host "Deleting Attachments..."
    $episodes | ForEach-Object {
        Delete-Attachments $_.FilePath $_.FileAttachments
    }
}

Write-Host "Done!"