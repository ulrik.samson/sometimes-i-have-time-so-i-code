
function clearAlarm() {
	chrome.action.setBadgeText({text: ''});
	chrome.alarms.clearAll();
	window.close();
}

function activatealarm() {
	chrome.storage.local.clear(function() {});
	chrome.storage.local.set({listarray: []}, function() {});
	chrome.action.setBadgeText({text: 'ON'});
	chrome.alarms.create({periodInMinutes: 1});
	window.close();
} 

document.getElementById('ON').addEventListener('click', activatealarm);
document.getElementById('OFF').addEventListener('click', clearAlarm);

