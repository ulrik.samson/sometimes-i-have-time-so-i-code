COLUMNS=`tput cols`
LINES=`tput lines`
column=`expr \( $COLUMNS - 40 \)`
tput sc

i=5
cat cat.txt | while read; do 
	tput cup $i $column
	((i=i+1))
	echo "$REPLY"; 
done

tput sgr0
tput rc
