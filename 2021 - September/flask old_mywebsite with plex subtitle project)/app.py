from flask import Flask, render_template, request, url_for, redirect, session
import os, json, copy, shutil, sys, pathlib, subprocess, pysubs2

#
#set app as a Flask instance
#
app = Flask(__name__, static_url_path='/static')

@app.route("/", methods=["GET"])
def home():
	return render_template('index.html')

@app.route('/<path:path>')
def static_file(path):
	return app.send_static_file(path)

@app.route('/resultat', methods=['GET', 'POST'])
def resultat():
	recherche = request.form.get("recherche")
	mainscript(recherche)
	return render_template('resultat.html', recherche=recherche)

def mainscript(recherche):

	#make directory great again
	directory = str(pathlib.Path().resolve())
	liste = []
	
	#check if file exist
	if os.path.isfile(directory+'\\static\\result\\'+recherche+'.json'):
		print ("File exist")
		return

	else:
		print ("File not exist")
		f = open(directory+'\\static\\result\\'+recherche+'.json', "w")
		f.write("{}")
		f.close()
	
	#for each srt/txt/ass in directory SRT, do function stringed
	for filename in os.listdir(directory+'\\srt'):
		try:
			subs = pysubs2.load('C:\\Users\\Franck\\Desktop\\4a\\testpy\\srt\\'+filename)
			superfunction(subs, recherche, os.path.join(directory+'\\srt', filename), filename)

		except:
			liste.append(filename)
			pass

def superfunction(subs, recherche, path, filename):
	#ouverture du srt
	file1 = open(path, "r")

	#variables super importantes
	index = 0 #index de la lines_to_print
	lines_to_print = [] #liste qui vas contenir tout les textes concerné
	data_set = {} #dico intermediaire
	dicte = [] #liste du total des resultat pour une film

	#pour chaque line du srt, matcher !!! et ajouter dans une liste les ligne qui contiennent le terme
	for line in subs:
		if recherche in line.text:
			if "/n" or "/N" in line.text :
				line.text = line.text.replace('\n', ' ')
				line.text = line.text.replace('\\N', ' ')
			data_set["content"] = line.text
			data_set["start"] = pysubs2.time.ms_to_str(line.start)
			data_set["end"] = pysubs2.time.ms_to_str(line.end)
			data_set_copy = copy.copy(data_set)
			lines_to_print.append(data_set_copy)
			print(line.text)

	if lines_to_print ==[]:
		return

	dicte = {filename : lines_to_print}
	
	with open(str(pathlib.Path().resolve())+'\\static\\result\\'+recherche+'.json') as f:
		data = json.load(f)

	data.update(dicte)

	with open(str(pathlib.Path().resolve())+'\\static\\result\\'+recherche+'.json', 'w') as f:
		json.dump(data, f, ensure_ascii=False, indent=4, sort_keys=True)
	
	f.close()

#
#main
#

if __name__ == "__main__":
	app.run(host="0.0.0.0", port=443, debug=True, ssl_context=('ssl.cer', 'key.key'))
