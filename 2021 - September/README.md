# Dialogue Picker

An unfinished project :/
There are two files. One in NodeJS, the other in Flask.
Both do the same work, but there's also a draft of a gif site, the concept:

I have a lot of movies stored, and I like to make gifs with extracts.
Problem, how ?

The idea is to extract the subtitle files (with mkvtool), then store them.
Then you can browse them to find a particular sentence.
For example, search for 'I am a dog' in all the subtitle files.
And finally, return the search result, and the user can select the desired extract and download it.

PS: in the Node site, there is also my CV, nothing crazy.

In short, it's not finished, but it's functional, I'll finish it one day.

### Requirements NodeJS

**create environement**

`mkdir site | cd site`

`npm init`

*modified package.lock with adding a nodeamon*

`"dev": "nodemon ./app.js"`

**install dependancy**

`npm install <dependancy>`

- nodemon
- express
- bootstrap-icons

**run**

`npm run dev`

### Requirements Flask

- ?
- probably `pip install +somelibrary`