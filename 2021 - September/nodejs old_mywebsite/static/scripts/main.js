(function ($) {
  $(document).ready(function(){

     $('.btn-warning').tooltip({title: "Hooray!", trigger: "hover"});    



    // hide .navbar first
    $(".navbar").hide();

    // fade in .navbar
    $(function () {
        $(window).scroll(function () {

                 // set distance user needs to scroll before we start fadeIn
            if ($(this).scrollTop() > $(window).height()-1) {
                $('.navbar').fadeIn();
            } else {
                $('.navbar').fadeOut();
            }
        });
    });

});
  }(jQuery));
