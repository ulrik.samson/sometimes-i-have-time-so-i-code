/**
 * Required External Modules
 */
const express = require("express");
const path = require("path");
const fs = require('fs');
const https = require('https');
const app = express();
const port = process.env.PORT || "443";

/**
 * HTTP Redirection
 */
const http = require('http');
const server = http.createServer((req, res) => {
  res.writeHead(301,{Location: `https://${req.headers.host}${req.url}`});
  res.end();
});
server.listen(8080);

/**
 *  App Configuration
 */
app.use(express.static(path.join(__dirname, "views")));
app.use(express.static(path.join(__dirname, "static")));


/**
 * Routes Definitions
 */
app.get("/", (req, res) => {
  res.render("", { title: "Home" });
});
app.get("/en", (req, res) => {
  res.render("en", { title: "Home" });
});

var getIP = require('ipware')().get_ip;
app.use(function(req, res, next) {
    var ipInfo = getIP(req);
    console.log(ipInfo);
    next();
});

/**
 * certificat
 */
var options = {
  key: fs.readFileSync('certificats/key.key'),
  cert: fs.readFileSync('certificats/ssl.cer')
};


/**
 * Server Activation
 */
https.createServer(options, app).listen(port, function() {console.log('Https App started');});