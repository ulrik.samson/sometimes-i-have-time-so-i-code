import random
import sys
import time

counter = 0

def randomed():
	with open("list.txt", "r") as file:
	    allText = file.read()
	    words = list(map(str, allText.split()))
	return random.choice(words)

def oui(motrandom,counter, start):
	
	if (time.time()-start) > 60:
		input('Vous avez gagné !!!') 
		return

	left, right = 'Mot : '+motrandom, 'Points : '+str(counter)
	print('|{}{}{}{}{}|'.format(left, ' '*(50-len(left+right)), right, ' '*(50-len(left+right)),time.time()-start))
	val = input("=> ")

	if motrandom==val:
		counter+=1
		oui(randomed(),counter, start)
	else :
		oui(motrandom,counter,start)

if __name__ == '__main__':
	start = time.time()
	oui(randomed(),counter, start)