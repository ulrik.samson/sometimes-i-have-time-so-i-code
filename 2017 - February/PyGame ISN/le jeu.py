
import pygame, sys, random, os
pygame.init()

X = 1000
Y = 500
worldx = 1500

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
plat = pygame.image.load('platform.png')
GREEN = (0, 155, 0)
MARRON = (91, 60, 17)
back1 = pygame.image.load('background.png')
back1tampon = back1
back2 = pygame.image.load('background2.png')
back2tampon = back2
screen = pygame.display.set_mode((X, Y))
pygame.display.set_caption("SLEEPER HERO")
clock = pygame.time.Clock()
cooldown = 1000
change = False
acc = 2
ani = 4
pok = False
pak = True
GAMEOVER = pygame.image.load('GAMEOVER.png')
son = pygame.mixer.Sound("jump.ogg")

WIN = pygame.image.load('win.png')
si_win = False

myfont = pygame.font.SysFont("ASMAN.TTF", 30)
textsurface = myfont.render('Jackson est un enfant, se soir il dors et fait un cauchemard', False, (255, 255, 255))
FPS = 60



cameraX = 0
cameraY = 0


class Platform():
    def __init__(self, sizex, sizey, posx, posy, color):
        self.image = pygame.surface.Surface((sizex, sizey))
        self.rect = self.image.get_rect(bottomleft=(posx, posy))    
        self.image.fill(color)
    def draw(self):
        
        screen.blit(self.image, self.rect)

        
   

class Player():
    def __init__(self):
        self.jump = False
        self.left = False
        self.right = False
        self.lives = 5
        self.heart = pygame.image.load('heart.png').convert_alpha()
        
        self.images = []
        for i in range(1,5):
            img = pygame.image.load(os.path.join('images','hero' + str(i) + '.png')).convert_alpha()
            
            
            
            self.images.append(img)
            self.image = self.images[0]
            self.rect = self.image.get_rect()
        
        self.y_speed = 0
        self.frame = 0
        
    def event(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP and self.on_ground():
                    self.jump = True
                    son.play()

            

        self.left = False
        self.right = False
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.left = True
            
        if keys[pygame.K_RIGHT]:
            self.right = True

    def move(self):
        if self.jump:
            self.y_speed = -24
            self.jump = False
        self.rect.bottom += self.y_speed
        if self.rect.x > 970:#stop animation quand colé au mue de droite
            self.right = False
        if self.rect.x < 1:#stop animation quand colé au mue de gauche
            self.left = False    
        if self.left:
            self.rect.x -= 5
        if self.right:
            self.rect.x += 5
        



         

        if self.on_ground():
            if self.y_speed >= 0:
                self.rect.bottom = p_rects[self.rect.collidelist(p_rects)].top + 1
                self.y_speed = 0
            else:
                self.rect.top = p_rects[self.rect.collidelist(p_rects)].bottom
                self.y_speed = 2
        else:
            self.y_speed += acc

          
        # moving right
        if self.right :
            self.frame += 1
            if self.frame > 3*ani:
                self.frame = 0
            self.image = self.images[(self.frame//ani)]

        # moving left
        if self.left :
            self.frame += 1
            if self.frame > 3*ani:
                self.frame = 0
            self.image = self.images[(self.frame//ani)]    



            
    def on_ground(self):
        collision = self.rect.collidelist(p_rects)
        if collision > -1:
            return True
        else:
            return False

    def draw(self):
        screen.blit(self.image, self.rect)
        for i in range(self.lives):
            screen.blit(self.heart, [i*20 + 20, 20])


class Enemy():
    def __init__(self):
        self.frame = 0
        self.images = []
        for i in range(1,5):
            img = pygame.image.load(os.path.join('images','monstre' + str(i) + '.png')).convert_alpha()
            
            
            self.images.append(img)
            self.image = self.images[0]
            self.rect = self.image.get_rect()
            
        

        
        self.x_speed = random.randint(2, 6)
        self.y_speed = 0
        

    def move(self):
        self.rect.x += self.x_speed
        if self.rect.x < 0-cameraX:
            self.x_speed *= -1
            
        if self.rect.x > 400-cameraX:
            self.x_speed *= -1
            
            

        if self.on_ground():
            self.rect.bottom = p_rects[self.rect.collidelist(p_rects)].top
            self.y_speed = 0
        else:
            self.y_speed += acc
        self.rect.bottom = 400

        self.hit()

        

        # moving right
        if self.move:
            self.frame += 1
            if self.frame > 3*ani:
                self.frame = 0
            self.image = self.images[(self.frame//ani)]

        
            
   

    def on_ground(self):
        collision = self.rect.collidelist(p_rects)
        if collision > -1:
            return True
        else:
            return False

    def hit(self):
        if player.rect.colliderect(self.rect) and player.lives >= 0:
            player.lives -= 1
                
                
            
            
            
        

    def draw(self):
        screen.blit(self.image, self.rect)


            
class Enemy2():
    def __init__(self):
        self.frame = 0
        self.images = []
        for i in range(1,5):
            img = pygame.image.load(os.path.join('images','monstres' + str(i) + '.png')).convert_alpha()
            
            
            self.images.append(img)
            self.image = self.images[0]
            self.rect = self.image.get_rect()
            
        

        
        self.x_speed = random.randint(2, 6)
        self.y_speed = 0
        

    def move(self):
        self.rect.x += self.x_speed 
        if self.rect.x < 300-cameraX:
            self.x_speed *= -1
        if self.rect.x > 900-cameraX:
            self.x_speed *= -1    
            

        if self.on_ground():
            self.rect.bottom = p_rects[self.rect.collidelist(p_rects)].top + 1
            self.y_speed = 0
        else:
            self.y_speed += acc
        self.rect.bottom = 458
        

        self.hit()

        

        # moving right
        if self.move:
            self.frame += 1
            if self.frame > 3*ani:
                self.frame = 0
            self.image = self.images[(self.frame//ani)]

        
            
   

    def on_ground(self):
        collision = self.rect.collidelist(p_rects)
        if collision > -1:
            return True
        else:
            return False

    def hit(self):
        if player.rect.colliderect(self.rect) and player.lives >= 0:
            player.lives -= 1
                

    def draw(self):
        screen.blit(self.image, self.rect)

# platform list:
platforms = []
platforms.append(Platform(X-80, 42, 0, Y, MARRON))
platforms.append(Platform(X+20, 42, X, Y, MARRON))
platforms.append(Platform(3000, 42, X+X+500, Y, MARRON))
platforms.append(Platform(200, 15, 600, 240, BLUE))
platforms.append(Platform(200, 15, 500, 340, BLUE))
platforms.append(Platform(400, 15, 2000, 350, BLUE))
platforms.append(Platform(100, 15, 900, 340, BLUE))
p_rects = [p.rect for p in platforms]




player = Player()
enemy = Enemy()
enemy2 = Enemy2()

while True:
    keys = pygame.key.get_pressed()
    clock.tick(FPS)
    screen.blit(back1,(0-cameraX,0-cameraY))
    time = pygame.time.get_ticks()
    player.event()
    player.move()
    enemy.move()
    
    
    if pak == True and pok == False:
        screen.fill((0,0,0))
        screen.blit(textsurface,(200, 250))
        
        
        if keys[pygame.K_RETURN]:
            pak = False

            
    if player.lives == 0 or pok == True:
        
            screen.blit(GAMEOVER, (0, 0))
            pok = True

                
            
        
        
    if pok == False and pak == False:
        player.draw()
        enemy.draw()
        back1 = back1
        for p in platforms :
            p.draw()
            
    
           
    
        
   # droite
    if player.rect.x > screen.get_width()/4*3 and cameraX < worldx-1000:
       cameraX += 5
       player.rect.x -= 5
       for p in platforms :
           p.rect.x -= abs(5)
       
       
    if player.rect.x > screen.get_width()/4*3 and cameraX > worldx-1000:
       cameraX += 0

   # gauche   
    if player.rect.x < screen.get_width()/4 and cameraX > 0:
       cameraX -= 5
       player.rect.x += 5
       for p in platforms :
           p.rect.x += abs(5)

    if player.rect.x > screen.get_width()/4 and cameraX < 0:
       cameraX += 0




    if player.rect.x > 970 and change == False:
        back1 = back2
        player.rect.x = 0
        player.rect.y = 0
        change = True


    if player.rect.y > Y:
        player.lives = 0
        
    if pok == True and keys[pygame.K_RETURN]:
        player.lives = 5
        player.y_speed = 0
        player.rect.y = 458
        player.rect.x = 0-cameraX
        pok = False
        
    
    if change == True:
        enemy2.draw()
        enemy2.move()
        


        
    if change == True and player.rect.x > 970 or si_win == True:
        screen.blit(WIN, (0, 0))
        si_win = True
        
    
    pygame.display.flip()
